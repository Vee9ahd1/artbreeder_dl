browser.runtime.onMessage.addListener(function(message){
	var downloading = fetch(message.url)
		.then(res => res.blob())
		.then(blob => window.URL.createObjectURL(blob))
		.then(objURL => browser.downloads.download({
			url: objURL,
			filename: message.filename,
			conflictAction: 'overwrite',
			saveAs: false
		}));
	function onStartedDownload(id) {
		console.log(`Download started: ${id}`);
	}
	function onFailed(error) {
		console.log(`Download failed: ${error}`);
	}
	downloading.then(onStartedDownload, onFailed);
	window.objURL.revokeObjectURL(objURL);
});
