(function() {
	var recordButton = document.createElement('INPUT');
	recordButton.type = 'checkbox';
	recordButton.id = 'recordButton';
	var recordLabel = document.createElement('LABEL');
	recordLabel.for = 'recordButton';
	recordLabel.innerHTML = 'Record Images';
	var filenameInput = document.createElement('INPUT');
	filenameInput.type = 'text';
	filenameInput.value = 'artbreeder';
	var recordDiv = document.createElement('DIV');
	recordDiv.appendChild(recordButton);
	recordDiv.appendChild(recordLabel);
	recordDiv.appendChild(filenameInput);
	var imageInfoContainer = document.getElementsByClassName('image_info_container')[0];
	imageInfoContainer.appendChild(recordDiv);
	const mainImage = document.getElementById("main_image");
	var i = 0;
	const callback = function(mutationList, observer) {
		for(let mutation of mutationList) {
			if(mutation.type === 'attributes') {
				browser.runtime.sendMessage({
					"url": mutation.target.src,
					"filename": `${filenameInput.value}_${("0000"+i).substr(-5)}.jpg`
				});
				i += 1;
			}
		}
	}

	const observer = new MutationObserver(callback);
	recordButton.addEventListener('change', function() {
		if(this.checked){
			observer.observe(mainImage, { attributes: true });
			i = 0;
		}
		else {
			observer.disconnect();
		}
	});
})();
